import { Role } from 'src/roles/entities/role.entity';

export class CreateUserDto {
  id: number;

  email: string;

  fullName: string;

  password: string;

  gender: string;

  roles: string;

  image: string;
}
